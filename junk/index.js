var React = require('react');
var ReactDOM = require('react-dom');
var App = require('./components/App');

ReactDOM.render(
    <App />,
    document.getElementById('like_button_container')
);