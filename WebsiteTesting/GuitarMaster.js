const canvas = document.getElementById("game");
canvas.width = window.innerWidth - 40;
canvas.height = window.innerHeight * .75;
const context = canvas.getContext("2d");
canvas.style.backgroundColor = 'white';
context.font = "250px Arial";
//optimize notes so they go away after off screen.
//make the user lose points or combo if missed
//make combo a thing
//remove double points
//check if at least one key is hit on strum
//make visuals more clean ie pics etc.
//visualy apealing full interface.

const diff = document.getElementById("difficulty");
const song = document.getElementById("song");
let speed = 2;
let comboMulti = 4;
var diffTempo = 12;
let lastnote = 0;
const frontsong = document.getElementById("frontsong");
//intitializing images to display on canvas
let wave = new Image(); wave.src = "./AlbumCovers/neckimg.jpg";
let blueicon = new Image(); blueicon.src = "./otherimgs/bluedot.png";
let redicon = new Image(); redicon.src = "./otherimgs/reddot.png";
let greenicon = new Image(); greenicon.src = "./otherimgs/greendot.png";
let orangeicon = new Image(); orangeicon.src = "./otherimgs/orangedot.png";
let yellowicon = new Image(); yellowicon.src = "./otherimgs/yellowdot.png";
let blackicon = new Image(); blackicon.src = "./otherimgs/blackdot.png";
let pauseButton = new Image(); pauseButton.src = "./otherimgs/pauseButton.png";
let AATW = new Image(); AATW.src = "./AlbumCovers/AATW.jpg";
let SONG2 = new Image(); SONG2.src = "./AlbumCovers/SONG2.jpg";

// arrays for storing currently pressed keys and all notes while in use
var time = 0;
var pressedKeys = [];
var notes = [];
var songs = [];
let strums = 0, hits = 0, comboScore = 1;
var score = 0, scoreTxt = "score: ", combo = 0, comboTXT = "combo: ";
var randy = 0, count=0, bottom =0,middle=canvas.width/2;
let fps, fpsInterval, startTime, now, then, elapsed;
let rand;
let ybar = canvas.height - 100;
var xbar1 = middle-145, xbar2 = middle-85, xbar3 = middle-25, xbar4 = middle+35, xbar5 = middle+95;
let center = ybar+25;
var pauseStatus = false;

//-----------Notes and song info---------------------//
function Blocks(id, row, img){
  this.row = row;
  this.img = img;
  this.id = id;
  this.hit = "false";
  if(row==1){
    this.xPos = middle-145;
  }
  if(row==2){
    this.xPos = middle-85;
  }
  if(row==3){
    this.xPos = middle-25;
  }
  if(row==4){
    this.xPos = middle+35;
  }
  if(row==5){
    this.xPos = middle+95;
  }
  this.yPos = -50;
  this.color = "red";
}

function SongInfo(title, img){
  this.t = title;
  this.img = img;
}

//-----------event listenters for key presses--------//

window.addEventListener("keydown", function(event){
  
  if (event.keyCode == 71 && pauseStatus == false) {
    frontsong.pause();
    startAnimating.pause();
    pauseStatus = true;
  }
  if (event.keyCode == 71 && pauseStatus == true) {
    animate.play();
    frontsong.play();
    pauseStatus = false;
  }
  if(event.keyCode == 32){
    strum();
  }
  if(event.keyCode == 81 || event.keyCode==87|| event.keyCode==69|| event.keyCode==82|| event.keyCode==84){
    pressedKeys[event.keyCode] = 1;
  }
});

window.addEventListener("keyup", function(event){
  if(event.keyCode == 81|| event.keyCode==87|| event.keyCode==69|| event.keyCode==82|| event.keyCode==84){
    pressedKeys[event.keyCode] = -1;
  }
});

//------------intitializing functions----------------//

function startSong(){
  document.getElementById("song-selection").style.display = "none";
  canvas.style.display = "block";

  switch(song.value) {
    case "AATW":
      songs[0] = new SongInfo("The Jimi Hendrix Experience - All Along the Watchtower", AATW);
      frontsong.src = "./Songs/AATW.mp3";
      context.font = "50x Arial";
      break;
    case "SONG2":
      songs[0] = new SongInfo("Blur - Song 2", SONG2);
      frontsong.src= "./Songs/SONG2.mp3";
      context.font = "50x Arial";
      break;
    case "UTB":
      songs[0] = new SongInfo("Red Hot Chili Peppers - Under the Bridge", UTB);
      context.font = "50x Arial";
      break;
    //more songs
    default:
      //not sure if we'll need this
  }
  setTimeout(() => {
  	context.clearRect(0,0,canvas.width, canvas.height);
    context.drawImage(songs[0].img, canvas.width/2-150, 80, 300,300);
    context.fillText(songs[0].t, 40,500);
    context.fillText("5",canvas.width/2-200,canvas.height/2);
  },100);
  setTimeout(() => {
  	context.clearRect(0,0,canvas.width, canvas.height);
    context.drawImage(songs[0].img, canvas.width/2-150, 80, 300,300);
    context.fillText(songs[0].t, 40,500);
    context.fillText("4",canvas.width/2-200,canvas.height/2);
  },1100);
  setTimeout(() => {
  	context.clearRect(0,0,canvas.width, canvas.height);
    context.drawImage(songs[0].img, canvas.width/2-150, 80, 300,300);
    context.fillText(songs[0].t, 40,500);
    context.fillText("3",canvas.width/2-200,canvas.height/2);
  },2200);
  setTimeout(() => {
  	context.clearRect(0,0,canvas.width, canvas.height);
    context.drawImage(songs[0].img, canvas.width/2-150, 80, 300,300);
    context.fillText(songs[0].t, 40,500);
    context.fillText("3",canvas.width/2-200,canvas.height/2);
  },3300);
  setTimeout(() => {
  	context.clearRect(0,0,canvas.width, canvas.height);
    context.drawImage(songs[0].img, canvas.width/2-150, 80, 300,300);
    context.fillText(songs[0].t, 40,500);
    context.fillText("1",canvas.width/2-200,canvas.height/2);
  },4400);
  setTimeout(() => {
    frontsong.play();
    startAnimating(60);
  },5200);

}

function pauseSong() {
  //pause song
  
}

function startAnimating(fps) {
	fpsInterval = 1000 / fps;
	then = Date.now();
	startTime = then;
	animate();
}

function animate() {
	requestAnimationFrame(animate);
	now = Date.now();
	elapsed = now - then;
	if (elapsed > fpsInterval) {
		then = now - (elapsed % fpsInterval);
		context.clearRect(0,0,canvas.width, canvas.height);
    time++;
    backgroundDisplay();
    keytest();
    showpress();
    makeNotes();
    showNotes();
    showElements();
	}
}

//------------functions to display stuff on screen---//

function backgroundDisplay(){
  context.drawImage(wave,0,0,canvas.width,canvas.height);
}

function showElements(){
  context.fillStyle = "orange";
  context.fillRect(20,20,380,190);
  context.fillRect(20,canvas.height-250,450,140);
  context.font = "40px Arial";
  context.fillStyle = "blue";
  let hitCount = "Hits: "; hitCount = hitCount + hits+"/"+count;
  let accuracyDisplay = "Accuracy: ";
  let percentAcc = parseFloat(hits/count*100).toFixed(2);
  accuracyDisplay = accuracyDisplay + percentAcc +"%";

  context.fillText(scoreTxt+score,40,60);
  context.fillText(hitCount,40,120);
  context.fillText(accuracyDisplay,40,180);
  context.fillText(comboTXT+combo,40,canvas.height-210);

  context.fillStyle = "plum";
  context.fillRect(40,canvas.height-190,300,60);

  context.drawImage(pauseButton, canvas.width-150, canvas.height-225, 75, 75);

  switch(comboScore) {
    case 1:
      context.fillStyle = "blue";
      break;
    case 2:
      context.fillStyle = "green";
      break;
    case 3:
      context.fillStyle = "red";
      break;
    case 4:
      context.fillStyle = "yellow";
      break;
    case 5:
      context.fillStyle = "white";
      break;
  }

  context.fillRect(50,canvas.height-180,(280/comboMulti)*(combo%comboMulti),40);

  if(comboScore==5) {
    context.fillRect(50,canvas.height-180,280,40);
  }

  context.font = "80px Arial";
  context.fillText(comboScore+"x",360,canvas.height-150);
  context.fillStyle = "blue";
  // if(song.value == "AATW"){
  //   context.drawImage(AATW, canvas.width-320, 20, 200,200);
  //   context.font = "17px Arial";
  //   context.fillText(song.innerText, canvas.width-480,260);
  // }

  switch(song.value) {
      case "AATW":
        context.drawImage(AATW, canvas.width-320, 20, 200,200);
        context.font = "17px Arial";
        context.fillText(song.innerText, canvas.width-480,260);
        break;
      case "SONG2":
        context.drawImage(SONG2, canvas.width-320, 20, 200,200);
        context.font = "17px Arial";
        context.fillText(song.innerText, canvas.width-480,260);
        break;
      default:
        //not sure if we need this
  }
}

function showNotes(){
  for(let i = lastnote;i<count;i++){
    if(notes[i].yPos > canvas.height-50){//combo break for beyond screen and garbage collection
      lastnote = i;
      notes[i].yPos=-49999;
      if(notes[i].hit=="false"){
        combo = 0; comboScore = 1;
      }
    }
    else{
      context.drawImage(notes[i].img,notes[i].xPos,notes[i].yPos,50,50);
      notes[i].yPos = notes[i].yPos + speed;
    }
  }
}

function showpress(){
  context.font = "30px Arial";
  if(pressedKeys[81]==1){context.drawImage(blueicon,xbar1,ybar,50,50);}
  if(pressedKeys[87]==1){context.drawImage(greenicon,xbar2,ybar,50,50);}
  if(pressedKeys[69]==1){context.drawImage(redicon,xbar3,ybar,50,50);}
  if(pressedKeys[82]==1){context.drawImage(yellowicon,xbar4,ybar,50,50);}
  if(pressedKeys[84]==1){context.drawImage(orangeicon,xbar5,ybar,50,50);}
}

//-----------backend stuff----------------------------//

function makeNotes(){
  randy = randy+1;
  if(diff.value=="Hard"){diffTempo = 45;}
  if(diff.value=="Medium"){diffTempo=66;}
  if(diff.value=="Easy"){ diffTempo=90;}
  if(randy%diffTempo == 0){
    //try random
    rand = Math.floor(Math.random()*2);
    if(rand==1){
      notes[count] = new Blocks(count,1,blueicon);
      count++;
    }
    rand = Math.floor(Math.random()*2);
    if(rand==1){
      notes[count] = new Blocks(count,2,greenicon);
      count++;
    }
    rand = Math.floor(Math.random()*2);
    if(rand==1){
      notes[count] = new Blocks(count,3,redicon);
      count++;
    }
    rand = Math.floor(Math.random()*2);
    if(rand==1){
      notes[count] = new Blocks(count,4,yellowicon);
      count++;
    }
    rand = Math.floor(Math.random()*2);
    if(rand==1 && diff.value=="Hard"){
      notes[count] = new Blocks(count,5,orangeicon);
      count++;
    }
  }
}
let minutes = 0, seconds=0, clock = "", progress = 0;
function keytest(){
  seconds = time/60;
  minutes = Math.floor(seconds/60);
  seconds = Math.floor(seconds%60);
  if(seconds<10){clock = minutes+":0"+seconds;}
  else{clock = minutes+":"+seconds;}
  context.fillStyle = "#c9c9c9";
  context.fillRect(xbar1,0,50,canvas.height);
  context.fillRect(xbar2,0,50,canvas.height);
  context.fillRect(xbar3,0,50,canvas.height);
  context.fillRect(xbar4,0,50,canvas.height);
  context.fillRect(xbar5,0,50,canvas.height);
  context.fillStyle = "white";
  context.fillRect(0,ybar,canvas.width,50);
  context.fillStyle = "aqua";
  context.fillRect(0,ybar,canvas.width*(time/(241*60)),50);
  context.fillStyle = "black";
  context.font = "30px Arial";
  context.fillText(clock,15,ybar+40);
  context.fillText("4:01",canvas.width-90,ybar+40);

}

function strum(){
  let flag = 0;
  let countPressed = 0;
  let strumhits = 0;
  if(pressedKeys[81]==1){countPressed++;}
  if(pressedKeys[87]==1){countPressed++;}
  if(pressedKeys[69]==1){countPressed++;}
  if(pressedKeys[82]==1){countPressed++;}
  if(pressedKeys[84]==1){countPressed++;}
  for(let i = 0;i<count;i++){
    if(notes[i].yPos>ybar-20 && notes[i].yPos<ybar+20 && notes[i].hit=="false"){
      if(notes[i].row==1 && pressedKeys[81]==1){
        score = score + 10*comboScore;
        notes[i].hit = "true";
        notes[i].img = blackicon;
        flag++; hits++; combo++; strumhits++;
      }
      if(notes[i].row==2 && pressedKeys[87]==1){
        score = score + 10*comboScore;
        notes[i].hit = "true";
        notes[i].img = blackicon;
        flag++; hits++; combo++;strumhits++;
      }
      if(notes[i].row==3 && pressedKeys[69]==1){
        score = score + 10*comboScore;
        notes[i].hit = "true";
        notes[i].img = blackicon;
        flag++; hits++; combo++;strumhits++;
      }
      if(notes[i].row==4 && pressedKeys[82]==1){
        score = score + 10*comboScore;
        notes[i].hit = "true";
        notes[i].img = blackicon;
        flag++; hits++; combo++;strumhits++;
      }
      if(notes[i].row==5 && pressedKeys[84]==1){
        score = score + 10*comboScore;
        notes[i].hit = "true";
        notes[i].img = blackicon;
        flag++; hits++; combo++;strumhits++;
      }

    }
  }
  if(flag==0){ //combo break goes here for miss
    combo = 0;
  }
  if(countPressed>strumhits){
    combo=0;
  }
  comboScore = 1 + ((combo/comboMulti)*10 - ((combo %comboMulti)/comboMulti)*10)/10;
  if(comboScore > 5){comboScore=5;}
}
