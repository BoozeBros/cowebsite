import React from 'react';
import { Box, Button,} from 'grommet';
import { Link } from 'react-router-dom';

function Sidebar() {
    return(
        <Box fill pad={{vertical:'medium'}} align='center'>

            <Link to ="/">
                <Button color="neutral-3">
                    <h3>Home</h3>
                </Button>
            </Link>
            <Link to ="/pathfinding">
                <Button color="neutral-3">
                    <h3>Pathfinding</h3>
                </Button>
            </Link>
            <Link to ="/Brady">
                <Button color="neutral-3">
                    <h3>Brady</h3>
                </Button>
            </Link>
            <Link to ="/Ethan">
                <Button color="neutral-3">
                    <h3>Ethan</h3>
                </Button>
            </Link>

        </Box>
    );
}

export default Sidebar;