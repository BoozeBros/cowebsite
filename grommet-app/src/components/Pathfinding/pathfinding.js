import React, { useState } from "react";
import { Box,Button, Heading, Grommet, Image} from 'grommet';
import {BrowserRouter, Routes, Route, Link} from 'react-router-dom';
import Dijkstra from './dijkstra';
import Sample from './sample';


function Pathfinding(){
    const [showD, setShowD] = useState(false);
    const [showS, setShowS] = useState(false);
    const [showA, setShowA] = useState(false);
    return(
        
            <Grommet flex>
                <Box flex align='center'>
                    <Heading level='2'>
                        Pathfinding Algorithms
                    </Heading>
                </Box>
                
                
                <Box flex align="center" pad="medium" direction="row" fill="horizontal">
                        <Box flex align="center" fill="horizontal">
                            <Button color="neutral-3">
                                <h3>Dijkstra's Algorithm</h3>
                            </Button>
                        </Box>
                        <Box flex align="center" fill="horizontal">
                            <Button color="neutral-3">
                                <h3>Sample Algorithm</h3>
                            </Button>
                        </Box>
                        <Box flex align="center" fill="horizontal">
                            A* Algorithm
                        </Box>
                </Box>
                
                <Box flex>
                    
                </Box>
            </Grommet>
        
    );
}

export default Pathfinding;