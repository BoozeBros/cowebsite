
import React, { useState } from "react";
import { Box, Button, Heading, Grommet, ResponsiveContext, Collapsible, Layer} from 'grommet';
import { BladesVertical, BladesHorizontal } from 'grommet-icons';


const AppBar = (props) => (
  <Box tag='header' direction='row' align='center' justify='between' background='brand' 
          pad={{ left: 'medium', right:'small', vertical:'small'}} elevation='medium' style={{ zindex:'1'}} {...props}/>
);

const theme = {
  global: {
    colors: {
      brand: '#ab22ab',
      sidebar: 'ab22ab',
    },
    font: {
      family: 'Roboto',
      size: '18px',
      height: '20px',
    }
  }
}


function Header() {
  const [showSidebar, setShowSidebar] = useState(false);
    return (
        <Grommet theme={theme} full>
          <ResponsiveContext.Consumer>
            {size => (
              <Box fill>
                <AppBar>
                  <Heading level='2' margin='none'>Brady and Ethan</Heading>
                  <Button icon={<BladesVertical/>} onClick={() => setShowSidebar(!showSidebar)} />
                </AppBar>
                <Box direction='row' flex overflow={{horizontal: 'hidden'}}>
    
                  <Box flex align='center' justify='center'>
                    Stuff
                  </Box>
    
                  
                {(!showSidebar || size!=='small') ? (
                  <Collapsible direction='horizontal' open={showSidebar}>
                    <Box flex width='medium' background='#ababde' elevation='small' align='center' justify='center'>
                      Sidebar Navigation
                    </Box>
                  </Collapsible>
                ):(
                  <Layer>
                    <Box background='brand' tag='header' justify='end' align='center' direction='row'>
                      <Button icon={<BladesHorizontal/>} onClick={() => setShowSidebar(false)}/>
                    </Box>
                    <Box fill background='#ababde' align='center' justify='center'>
                      Full sidebar nav for mobile
                    </Box>
                  </Layer>
                )}
                </Box>
              </Box>
            )}
          </ResponsiveContext.Consumer>
        </Grommet>
      );
};

export default Header;