import React, { useState } from "react";
import { Box, Grommet, ResponsiveContext, Collapsible, Layer, Button, Heading} from 'grommet';
import { BladesVertical, BladesHorizontal } from "grommet-icons";
import {BrowserRouter, Routes, Route} from 'react-router-dom';
import Home from './components/Home/home';
import Sidebar from "./components/Sidebar/sidebar";
import Brady from "./components/Brady/brady";
import Ethan from "./components/Ethan/ethan";
import Pathfinding from "./components/Pathfinding/pathfinding";

const AppBar = (props) => (
  <Box tag='header' direction='row' align='center' justify='between' background='brand' 
          pad={{ left: 'medium', right:'small', vertical:'small'}} elevation='medium' style={{ zindex:'1'}} {...props}/>
);

const theme = {
  global: {
    colors: {
      brand: '#ab22ab',
    },
    font: {
      family: 'Roboto',
      size: '18px',
      height: '20px',
    }
  }
}

function App() {
  const [showSidebar, setShowSidebar] = useState(false);
  return (
    <Grommet full>
      <BrowserRouter>
        <Grommet theme={theme} full>
          <ResponsiveContext.Consumer>
            {size => (
              <Box fill>
                <AppBar>
                  <Heading level='2' margin='none'>Brady and Ethan</Heading>
                  <Button icon={<BladesVertical/>} onClick={() => setShowSidebar(!showSidebar)} />
                </AppBar>
                <Box direction='row' flex overflow={{horizontal: 'hidden'}}>
    
                  <Box flex>
                    <Routes>
                      <Route path='/' element={<Home/>}/>
                      <Route path='/brady' element={<Brady/>}/>
                      <Route path='/ethan' element={<Ethan/>}/>
                      <Route path='/pathfinding' element={<Pathfinding/>}/>
                    </Routes>
                    
                  </Box>
    
                  
                {(!showSidebar || size!=='small') ? (
                  <Collapsible direction='horizontal' open={showSidebar}>
                    <Box flex width='small' background='#ababde' elevation='small' align='center' justify='center'>
                      <Sidebar/>
                    </Box>
                  </Collapsible>
                ):(
                  <Layer>
                    <Box background='brand' tag='header' justify='end' align='center' direction='row'>
                      <Button icon={<BladesHorizontal/>} onClick={() => setShowSidebar(false)}/>
                    </Box>
                    <Box fill background='#ababde' align='center' justify='center'>
                      <Sidebar/>
                    </Box>
                  </Layer>
                )}
                </Box>
              </Box>
            )}
          </ResponsiveContext.Consumer>
        </Grommet>
      </BrowserRouter>
    </Grommet>
  );
}

export default App;
