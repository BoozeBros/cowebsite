
var dealBut = document.getElementById("deal");
var cardTable = document.getElementById("laidCards");
var ptotal = document.getElementById("playerTotal");
var ptotal2 = document.getElementById("playerTotal2");
var dtotal = document.getElementById("dealerTotal");
var playButs = document.getElementById("playButs");
var hit = document.getElementById("hitBut");
var hold = document.getElementById("holdBut");
var wl = document.getElementById("winStatus");
var money = document.getElementById("money");
var betIn = document.getElementById("bet");
var double = document.getElementById("doubleBut");
var split = document.getElementById("splitBut");
var secondHand = document.getElementById("secondHand"); secondHand.style.display = "none";
var total2 = document.getElementById("secondTotal");   total2.style.display = "none";
var next = document.getElementById("nextCard"); next.style.display = "none";

var pHand = [];
var pHand2= [];
var dHand = [];
var dScore = 0;
var pScore2 = 0;
var pScore = 0;
var chips = 1000;
var iBet=0;
var bet = 0;
var bet2=0;
var p1,p2,d1,d2;
var playerBlackJack;
var dealerBlackJack;
var splityn = 0;
var hand2 =0;
var hand1bust=0;
var hand2bust=0;

function drawCard(){
  return Math.floor(Math.random() * 52);
}


dealBut.onclick = function(){
  totalBet=0;
  hand1bust=0;
  secondHand.style.display = "none";
  total2.style.display = "none";
  playerBlackJack = 0;
  dealerBlackJack = 0;
  hand2bust=0;
  splityn = 0;
  hand2=0;
  wl.innerHTML = "";
  bet=0;
  bet2=0;
  bet = betIn.value;
  iBet = bet;
  wl.innerHTML = bet;
  if(bet>chips){
    wl.innerHTML = "Bet Lower";
    return;
  }
  chips -= bet;
  updateMoney();
  dScore = 0;
  pScore = 0;
  pScore2 = 0;
  pHand = [];
  pHand2 = [];
  dHand = [];
  for(var i=0;i<7;i++){
    var did = "dc" + i;
    var pid = "pc" + i;
    var pid2 = "p2" + i;
    var setImg = document.getElementById(did);
    setImg.src="./bjCards/back.png";
    var setImg = document.getElementById(pid);
    setImg.src="./bjCards/back.png";
    var setImg = document.getElementById(pid2);
    setImg.src="./bjCards/back.png";
  }
  ///initial deal
  p1 = drawCard();
  d1 = drawCard();
  p2 = drawCard();
  d2 = drawCard();
  addCardPlayer(p1, 0);
  addCardDealer(d1, 0);
  addCardPlayer(p2, 1);
  setCounts();
  if(playerBlackJack==1){
    gameOver();
    return;
  }
  else if(d1>35){
    if(d2<4){
      dealerBlackJack = 1;
      addCardDealer(d2,1);
      gameOver();
      return;
    }
  }
  else if(d1<4){
    if(d2>35){
      dealerBlackJack = 1;
      addCardDealer(d2,1);
      gameOver();
      return;
    }
  }
  setCounts();
  //hide and show new buttons
  dealBut.style.display = "none";
  playButs.style.display = "inline";
  betIn.style.display = "none";

};
hit.onclick = function(){
  if(hand2==0){
    addCardPlayer(drawCard(),pHand.length);
    setCounts();
  }
  else{
    addCardPlayer2(drawCard(),pHand2.length);
    setCounts();
  }
}
hold.onclick = function(){
  playerHold();
}
double.onclick = function(){
  if(chips>iBet&&pHand.length==1){
    chips -= iBet;
    bet = (iBet*2);
    wl.innerHTML=bet;
    updateMoney();
    addCardPlayer(drawCard(),1);
    setCounts();
    playerHold();
  }
  else if (chips>iBet&&pHand.length==2&&bet==iBet&&hand2==0) {
    chips-=iBet;
    bet = (iBet*2);
    wl.innerHTML=bet;
    updateMoney();
    addCardPlayer(drawCard(),2);
    setCounts();
    playerHold();
    return;
  }
  else if (chips>iBet&&pHand2.length==1) {
    chips-=iBet;
    bet2 = (iBet*2);
    wl.innerHTML=bet;
    updateMoney();
    addCardPlayer2(drawCard(),1);
    setCounts();
    playerHold();
  }
  else{return;}
}
split.onclick = function(){
  if(Math.floor(p1/4)==Math.floor(p2/4)){
    //secondHand.style.display = "block";
    pHand = [];
    addCardPlayer(p1,0);
    addCardPlayer2(p2,0);
    var setImg = document.getElementById("pc1");
    setImg.src="./bjCards/back.png";
    splityn =1;
    setCounts();
    chips -= bet;
    bet2 = iBet;
    total2.style.display = "inline";
    secondHand.style.display = "inline";
    updateMoney();
  }
  else if (p1>35&&p2>35) {
    //secondHand.style.display = "block";
    pHand = [];
    addCardPlayer(p1,0);
    addCardPlayer2(p2,0);
    var setImg = document.getElementById("pc1");
    setImg.src="./bjCards/back.png";
    splityn =1;
    setCounts();
    chips -= bet;
    bet2=iBet;
    total2.style.display = "inline";
    secondHand.style.display = "inline";
    updateMoney();
  }
  else{
    return;
  }
}

function addCardPlayer(card, pos){
  addToHand(card, "p");
  var id = "pc" + pos;
  var pic = "./bjCards/" + card + ".png";
  var setImg = document.getElementById(id);
  setImg.src = pic;
}
function addCardPlayer2(card, pos){
  addToHand(card, "p2");
  var id = "p2" + pos;
  var pic = "./bjCards/" + card + ".png";
  var setImg = document.getElementById(id);
  setImg.src = pic;
}
function addCardDealer(card, pos){
  addToHand(card, "d");
  var id = "dc" + pos;
  var pic = "./bjCards/" + card + ".png";
  var setImg = document.getElementById(id);
  setImg.src = pic;
}
function addToHand(card, pd){
  var setCard;
  var num = Math.floor(card/4);
  switch(num){
    case 0:
      setCard = 1;
      break;
    case 1:
      setCard = 2;
      break;
    case 2:
      setCard = 3;
      break;
    case 3:
      setCard = 4;
      break;
    case 4:
      setCard = 5;
      break;
    case 5:
      setCard = 6;
      break;
    case 6:
      setCard = 7;
      break;
    case 7:
      setCard = 8;
      break;
    case 8:
      setCard = 9;
      break;
    case "back":
      setCard = 0;
      break;
    default:
      setCard = 10;
      break;
  }
  if(card == "back"){setCard=0;}
  if(pd=="p"){
    pHand.push(setCard);
  }
  else if(pd=="p2"){
    pHand2.push(setCard);
  }
  else{
    dHand.push(setCard);
  }
}
function setCounts(){
  var ptot1 = 0;
  var ptot2 = 0;
  var ptot21 = 0;
  var ptot22 = 0;
  var dtot1 = 0;
  var dtot2 = 0;
  pHand.forEach((item, i) => {
    if(item==1){
      ptot1+= item;
      ptot2+= item + 10;
    }
    else{ptot1 += item; ptot2 += item;}
    if(ptot2>21){
      ptot2 -= 10;
    }
  });
  if(ptot1 == ptot2){
    ptotal.innerHTML = ptot2;
    pScore = ptot2;
  }
  else if(ptot1>21){
    ptotal.innerHTML = "bust";
    if(splityn==0){
      pScore = ptot1;
      gameOver();
    }
    else if(hand1bust==0){
      playerHold();
      hand1bust=1;
      return;
    }
  }
  else if(ptot2==21&&pHand.length==2&&pHand2.length<1){
    playerBlackJack = 1;
    return;
  }
  else{
    ptotal.innerHTML = ptot1 + " or " + ptot2;
    pScore = ptot2;
  }
if(hand2bust==0){
  pHand2.forEach((item, i) => {
    if(item==1){
      ptot21+= item;
      ptot22+= item + 10;
    }
    else{ptot21 += item; ptot22 += item;}
    if(ptot22>21){
      ptot22 -= 10;
    }
  });
  if(ptot21 == ptot22){
    ptotal2.innerHTML = ptot22;
    pScore2 = ptot21;
  }
  else if(ptot21>21){
    ptotal2.innerHTML = "bust";
    pScore2 = ptot21;
    splityn=0;
    hand2bust=1;
    playerHold();
  }
  else{
    ptotal2.innerHTML = ptot21 + " or " + ptot22;
    pScore2 = ptot21;
  }
}

  dHand.forEach((item, i) => {
    if(item==1){
      dtot1+= item;
      dtot2+= item + 10;
    }
    else if(dtot2==21&&dHand.length==2){
      dScore=dtot1;
      dealerBlackJack = 1;
      gameOver();
      return;
    }
    else{dtot1 += item; dtot2 += item;}
    if(dtot2>21){
      dtot2 -= 10;
    }
  });
  if(dtot1 == dtot2){
    dtotal.innerHTML = dtot1;
    dScore = dtot1;
  }
  else{
    if(dtot1>21){
      dtotal.innerHTML = dtot1;
      dScore = dtot1;
    }
    else{
      dtotal.innerHTML = dtot1 + " or " + dtot2;
      dScore = dtot1;
    }
  }
}

//dealer draws
function playerHold(){
    if(splityn==0){
    playButs.style.display = "none";
    for(var i = 1;i<7;i++){
      if(dHand.length==1){
        addCardDealer(d2,1);
      }
      else{
        addCardDealer(drawCard(),i);
      }
      setCounts();
      if(dScore>21){
        gameOver();
        return;
      }
      if(dScore>16){
        gameOver();
        return;
      }
    }
  }
  else if(hand2==1){
    splityn=0;
    playerHold();
  }
  else{
    hand2=1;
    wl.innerHTML = "hand 2";
    return;
  }
}
function gameOver(){
  //check wins
  var endMessage = "";
  var earnings = 0;
  if(playerBlackJack==1){
    endMessage = "Player BlackJack!";
    earnings += bet*2.5;
  }
  else if(dealerBlackJack){
    endMessage = "Dealer BlackJack :("
  }
  else{
    var result1 = compareHands(dScore,pScore);
    switch(result1){
      case -1:
        endMessage += "You Lose Hand:1";
        break;
      case -2:
        endMessage += "You Bust Hand:1";
        break;
      case 0:
        endMessage += "You Push Hand:1";
        earnings = (bet*1);
        break;
      case 2:
        endMessage += "DealerBust Hand:1";
        earnings = (bet*2);
        break;
      case 1:
        endMessage += "You Win Hand:1";
        earnings = (bet*2);
        break;
    }

    if(hand2==1){
      var result2 = compareHands(dScore,pScore2);
      switch(result2){
        case -1:
          endMessage += " - You Lose Hand:2";
          break;
        case -2:
          endMessage += " - You Bust Hand:2";
          break;
        case 0:
          endMessage += " - You Push Hand:2";
          earnings += (bet2*1);
          break;
        case 2:
          endMessage += " - DealerBust Hand:2";
          earnings += (bet2*2);
          break;
        case 1:
          endMessage += " - You Win Hand:2";
          earnings += (bet2*2);
          break;
      }
    }
  }
  if(earnings>0){
    endMessage += " - Win:" + earnings + " Chips";
  }
  chips+=earnings;
  wl.innerHTML = endMessage;
  dealBut.style.display = "inline";
  playButs.style.display = "none";
  betIn.style.display = "inline";
  updateMoney();

}
function compareHands(dh, ph){
  if(ph>21){
    return -2;
  }
  else if (dh>21) {
    return 2;
  }
  else if(dh>ph){
    return -1;
  }
  else if(dh<ph){
    return 1;
  }
  return 0;
}
function updateMoney(){
  money.innerHTML = chips;
}

function odds(){
  //odds dealer beats you on next draw
  return
}
